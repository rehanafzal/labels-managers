<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Scrape 
{	
	 public $proxies=array();		
	 function getTextBetweenTags($string, $tagname) // this will return only Tags Info like <h1></h1>
	 {
	  $pattern = "/<$tagname ?.*>(.*)<\/$tagname>/";
	  preg_match_all($pattern, $string, $matches);
	  return $matches[0];
	 }
	 function removespace($text)
	 {
		 return str_replace(" ","",$text);
	 }
	function removeWhiteSpace($text) // Remove White Space
	{
		 $text = preg_replace('/[\t\n\r\0\x0B]/', ' ', $text);
		 $text = preg_replace('/([\s])\1+/', ' ', $text);
		 $text = trim($text);
		 return $text;
	}
	function ElementValue($arr,$ele)
	{
		return $arr[$ele];
	}
	function ExplodeBy($sep,$str)
	{
		return explode($sep,$str);
	}
	function GetLastElementAarry($array)
	{
		return $array[count($array)-1];
	}
	function GetImageSrc($str)
	{
		preg_match_all('/< *img[^>]*src *= *["\']?([^"\']*)/i', $str, $images);
		return $images;
	}
	function HeadingText($str)
	{
		$headings="";
		$newstr = preg_match_all('|<h[^>]+>(.*)</h[^>]+>|iU', $str, $headings);
		return $headings[1][0];
	}
	function GetListOfEmail($text)
	{
		$arr=array();
		
		$res = preg_match_all("/[a-z0-9]+[_a-z0-9\.-]*[a-z0-9]+@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/i",$text,$matches);

		if($res) 
		{
			foreach(array_unique($matches[0]) as $email) 
			{
				$arr[]=$email;
			}
		} else
		{
			return $arr;
		}
		return $arr;	
	}	
	function FinalOptimize($ar)
	{
		$nomiarray=array();
		$isfound=0;
		$alist=array('.css','.js','.png','.jpg','.ico');
		foreach($ar as $url):
			$isfound=0;
			for($i=0;$i<count($alist);$i++)
			{			
				if(strpos($url,$alist[$i])>0) 
				{
					$isfound=1;
				}	
			}
			if($isfound==0)
			{
				$nomiarray[]=str_replace(array("'"),"",$url);
			}
		endforeach;
		return array_unique($nomiarray);
	}
	function GetSiteHost($url)
	{
		$info=parse_url($url);
		if(isset($info['host']) && $info['host']!="")
		{
			return $info['host'];
		} else
		{
			$url="http://".$url;
			$info=parse_url($url);
			return $info['host'];
		}
	}
	function MakeitfullURL($info,$site)
	{
		if(isset($info['scheme']) && $info['scheme']!="" && isset($info['path']) && $info['path']!="" && isset($info['host']) && $info['host']!="")
		{
			return true;
		}
	}
	function GetFullPageContent2($url)
	{
		return file_get_contents($url);
	}
	function getAUrls($c)
	{
		$regex = "/(?<=href=(\"|'))[^\"']+(?=(\"|'))/";
		preg_match_all($regex, $c, $matches);
		return ($matches[0]);
	}
	function getHttpUrls($c)
	{
		$regex = '/https?\:\/\/[^\" ]+/i';
		preg_match_all($regex, $c, $matches);
		return ($matches[0]);
	}
	function pre($ar)
	{
		echo "<pre>";
		print_r($ar);
		echo "</pre>";
		echo "<br/>";
	}
	function GetDevContent2($output,$dst,$dend)
	{
		$start = strpos($output, $dst);
		$end = strpos($output, $dend, $start);
		$length = $end-$start;
		$nutput = substr($output, $start, $length);
		return $nutput;
	}
	public function tdval($link)
	{
		$link = preg_replace('/<td.*?>/i', '', $link);
		$link = preg_replace('/<\/td>/i', '', $link);
		$Content = preg_replace("/&#?[a-z0-9]+;/i","",$link);
		$Content=strip_tags($Content);
		return trim(preg_replace('/\s\s+/', '',$Content));
	}
	public function linkname($link)
	{
		$link = preg_replace('/<a.*?>/i', '', $link);
		$link = preg_replace('/<\/a>/i', '', $link);
		return $link;
	}
	public function PageContentCurlProxy($url)
	{		
		$ch = curl_init();			
		if($ch === false)
		{
			die('Failed to create curl object');
		}				
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$rand=rand(0,2);
		curl_setopt($ch, CURLOPT_PROXY, $this->proxies[$rand]);
		curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
		$data = curl_exec($ch);
		$res=curl_getinfo($ch);		
		curl_close($ch);
		if($res['primary_ip']=="" || $data=="")
		{
			$this->PageContentCurlProxy($url);
		}		
		return $data; 
	}
	public function pagecontentcurl($url)
	{		
		$crl = curl_init();
		curl_setopt($crl, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
		curl_setopt($crl, CURLOPT_URL, $url);		
		curl_setopt($crl, CURLOPT_PROXYTYPE, 'HTTP');
		curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);				
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, 5);
		$ret = curl_exec($crl);
		curl_close($crl);
		return $ret;		
	}
	function get_final_url( $url, $timeout = 5 )
	{
		$url = str_replace( "&amp;", "&", urldecode(trim($url)) );
		$cookie = tempnam ("/tmp", "CURLCOOKIE");
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_ENCODING, "" );
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
		curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
		curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
		$content = curl_exec( $ch );
		$response = curl_getinfo( $ch );
		curl_close ( $ch );
		if ($response['http_code'] == 301 || $response['http_code'] == 302)
		{
			ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
			$headers = get_headers($response['url']);

			$location = "";
			foreach( $headers as $value )
			{
				if ( substr( strtolower($value), 0, 9 ) == "location:" )
					return get_final_url( trim( substr( $value, 9, strlen($value) ) ) );
			}
		}
		if (    preg_match("/window\.location\.replace\('(.*)'\)/i", $content, $value) ||
				preg_match("/window\.location\=\"(.*)\"/i", $content, $value)
		)
		{
			return get_final_url ( $value[1] );
		}
		else
		{
			return $response['url'];
		 }
	}
	function get_domain($url)
	{
	  $pieces = parse_url($url);
	  $domain = isset($pieces['host']) ? $pieces['host'] : '';
	  if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
		return "site:".$regs['domain'];
	  }
	  return false;
	}
	function GetDevContent($output,$dst,$dend)
	{
		$start = strpos($output, $dst);
		$end = strpos($output, $dend, $start);
		$length = $end-$start;
		$nutput = substr($output, $start, $length);
		return strip_tags($nutput);
	}
	function GetMetaType($des)
	{
		$listometatype=array('name="description"');
		$listometatyped=array('description');
		for($i=0;$i<count($listometatype);$i++)
		{
			$res=str_replace(strtolower($listometatype[$i]),"",strtolower($des),$countd);
			if($countd>0)
			{
				return $listometatyped[$i];
				break;
			}
		}
		return -1;
	}
	function GetSiteMeta($output)
	{
		$resultarray=array();
		$start = strpos($output, '<head>');
		$end = strpos($output, '</head>', $start);
		$length = $end-$start;
		$nutput = substr($output, $start, $length);
		$nres=explode('<meta ',$nutput);
		for($i=0;$i<count($nres);$i++)
		{			
			if(str_replace(" ","",$nres[$i])!="")
			{
				$res=$this->GetMetaType($nres[$i]);
				if($res!=-1)
				{
					$start = strpos($nres[$i], 'content=');
					$end = strpos($nres[$i], '>', $start);
					$length = $end-$start;
					$nutput = strip_tags(substr($nres[$i], $start, $length));
					$resultarray['descriptiontext']=str_replace(array($res,'/>','/','content=','"'),"",$nutput);
				}
			}			
		}		
		return $resultarray;
	}
	function update($selectquery)
		{
		if($result=mysql_query($selectquery))
			{
				return true;
			}		
			return false;
		}
	function delete($selectquery)
		{
		if($result=mysql_query($selectquery))
			{
				return true;
			}		
			return false;
		}
	public function ArrayToUpdate($tbl,$array, $where) 	
	{
        $edited = 'User Ammended';
        //$array['password'] = $this->hashPassword($array['password']);
        /*Assuming array keys are = to database fileds*/
        if (count($array) > 0) {
            foreach ($array as $key => $value) {

                $value = mysql_real_escape_string($value); // this is dedicated to @Jon
                $value = "'$value'";
                $updates[] = "$key = $value";
            }
        }
        $implodeArray = implode(', ', $updates);
        $sql = ("UPDATE $tbl SET $implodeArray WHERE $where");
        return $sql;
	}
	function select($selectquery)
	{
	/*
	$listofcar=$obj->selectMulti("SELECT * FROM ".TBL_CARS." WHERE catid='".$catdes['des']."'");
	for($i=0;$i<count($listofcar);$i++)
	{
	<?php echo $listofcar[$i]['name'];?>
	*/
	if($result=mysql_query($selectquery))
		{
		return mysql_fetch_assoc($result);
		}
	else
		return false;
	}
	function countrecord($countquery)
	{
		if($result=mysql_query($countquery))
		{
			return	mysql_num_rows($result);
		}
		else
		{
			return false;
		}
	}
	function isfburl($aurl)
	{
		$fbUrlCheck = '/^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/';
		$secondCheck = '/home((\/)?\.[a-zA-Z0-9])?/';	
		foreach($aurl as $url):
		$validUrl = $url;
		if(preg_match($fbUrlCheck, $validUrl) == 1 && preg_match($secondCheck, $validUrl) == 0) 
		{
			return $url;
		}
		endforeach;
		return "";		
	}
	function selectMulti($querry_sql)
	{   
		if((@$result = mysql_query ($querry_sql))==FALSE)
		{
			$result=mysql_query ($querry_sql) or die(mysql_error());	
		}   
		else
		{	
			$count = 0;
			$data = array();
			while ( $row = mysql_fetch_array($result)) 
			{
				$data[$count] = $row;
				$count++;
			}
				return $data;
		}
	}

	function istwurl($aurl)
	{
		$fbUrlCheck = '/^(https?:\/\/)?(www\.)?twitter.com\/[a-zA-Z0-9(\.\?)?]/';
		$secondCheck = '/home((\/)?\.[a-zA-Z0-9])?/';	
		foreach($aurl as $url):
		$validUrl = $url;
		if(preg_match($fbUrlCheck, $validUrl) == 1 && preg_match($secondCheck, $validUrl) == 0) 
		{
			return $url;
		}
		endforeach;
		return "";				
	}
}