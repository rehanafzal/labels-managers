<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller 
{
	public function logout()
	{
		$Item_Array=array('user_type'=>'','user_id'=>'','user_email'=>'');
		$this->session->set_userdata($Item_Array);
		$this->session->set_flashdata('errormsg', 'Account logout successfully!');
		$this->session->set_flashdata('msgtype', 'success');
		redirect(site_url('login'),"refresh");
		exit;
	}
	public function login()
	{
		$un  =	trim($this->input->post('un'));
		$psw =	trim($this->input->post('psw'));
		if($un=="" || $psw=="")
		{
			$this->session->set_flashdata('errormsg', 'Invalid Username Or Password!');
			$this->session->set_flashdata('msgtype', 'danger');
			redirect(site_url('login'));
			exit;			
		}else
		{
			$dataarray=array('user_name'=>$un,'user_password'=>md5($psw),'user_active'=>1);
			$res=$this->Common->GetSingleRow('tbl_users',$dataarray,array('user_type','user_id','user_email'),'user_id',0);
			if($res['user_type']!="" && $res['user_id']!="")
			{
				$this->session->set_userdata($res);
				redirect(site_url('dashboard'),"refresh");
				exit;
			} else
			{
				$this->session->set_flashdata('errormsg', 'Invalid Username Or Password!');
				$this->session->set_flashdata('msgtype', 'danger');
				redirect(site_url('login'));
				exit;			
			}
			//GetSingleRow
		}
	}
}
