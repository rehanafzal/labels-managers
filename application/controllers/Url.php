<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Url extends CI_Controller 
{
	public $content=array();
	public function __construct()
	{
		parent::__construct();
		$this->Common->Islogin();
		$erromsg=$this->session->flashdata('errormsg');
		$msgtype=$this->session->flashdata('msgtype');
		$this->content['mainmenu']=$this->uri->segment(1);
		$this->content['submenu']=$this->uri->segment(2);
		$this->content['websites']=$this->Common->GetAllRow('tbl_website_list','row_id >0',"*","row_id","","DESC","");
		if($erromsg!="")
		{
			$errormsg='<div class="row"><div class="col-sm-12"><div class="alert alert-'.$msgtype.'">'.$erromsg.'</div></div></div>';
			$this->content['errormsg']=$errormsg;
		}else
		{
			$this->content['errormsg']='';
		}
	}	 
	public function index()
	{
		$this->content['title']="Labels list";
		$where='tbl_url_list.row_id >0';
		$join=array();
		$join[]=array('tbl_website_list','tbl_url_list.website_id = tbl_website_list.row_id','LEFT');
		$this->content['data']=$this->Common->GetAllRowJoin('tbl_url_list',$where,"tbl_url_list.*,tbl_website_list.website_name",$join,"tbl_url_list.website_id","","ASC","");
		//GetAllRow($tbl,$where="",$select="*",$orderby="",$numrow="",$ordertype="",$limit="")
		$this->load->view('url/url_list',$this->content);
	}
	public function delete($row_id)
	{
		$this->Common->Delrow('tbl_url_list',array('row_id'=>$row_id));
		$this->session->set_flashdata('errormsg', 'URL Deleted has been updated successfully!');
		$this->session->set_flashdata('msgtype', 'success');
		redirect(site_url('url'),"refresh");
		exit;
	}		
	public function edit($row_id)
	{
		$daatarray=array();
		$add=$this->input->post('isubmit');
		if($add==1)
		{
			 $daatarray['website_url']=$this->input->post('account_name');			  
			 $daatarray['website_id']=$this->input->post('website_name');			  
			 $this->Common->Update('tbl_url_list',$daatarray,array('row_id'=>$row_id));			
			 $this->session->set_flashdata('errormsg', 'URL has been updated successfully!');
  			 $this->session->set_flashdata('msgtype', 'success');
			 redirect(site_url('url'),"refresh");
			 exit;
		}			
		$this->content['data']=$this->Common->GetSingleRow('tbl_url_list',array('row_id'=>$row_id),"*","",0);
		$this->content['title']="Edit Url";		
		$this->load->view('url/add',$this->content);
	}
	public function add()
	{
		$add=$this->input->post('isubmit');
		if($add==1)
		{
			 $label			=	explode(";",$this->input->post('account_name'));	
			 $website_name	=	$this->input->post('website_name');
			 if(count($label)<0)
			 {
				 $this->session->set_flashdata('errormsg', 'label Name is required!');
				 $this->session->set_flashdata('msgtype', 'danger');
				 redirect(site_url('url'));
				 exit;
			 }
			 $labelc=0;
			 //$this->Common->pre($label); exit;
			 for($j=0;$j<count($label);$j++)
			 {
					$daatarray=array();					
					$daatarray['website_id']=$website_name;
					$daatarray['website_url']=$label[$j];
					if($this->Common->GetSingleRow('tbl_url_list',$daatarray,"row_id","row_id",0)<1)
					{
						$daatarray['website_url']=$this->Common->removeWhiteSpace($label[$j]);
						$this->Common->Insert('tbl_url_list',$daatarray);
						$labelc++;
					}
			 }
			 $this->session->set_flashdata('errormsg', '$labelc New URL(s) has been added successfully!');
  			 $this->session->set_flashdata('msgtype', 'success');
			 redirect(site_url('url'),"refresh");
			 exit;
		}			
		$this->content['title']="Add URL";		
		$this->load->view('url/add',$this->content);
	}
}