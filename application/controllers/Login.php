<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
	public $dataarray=array();
	public function __construct()
	{
		parent::__construct();
		//$this->Common->Islogin();
		$erromsg=$this->session->flashdata('errormsg');
		$msgtype=$this->session->flashdata('msgtype');
		if($erromsg!="")
		{
			$errormsg='<div class="row"><div class="col-sm-12"><div class="alert alert-'.$msgtype.'">'.$erromsg.'</div></div></div>';
			$this->dataarray['errormsg']=$errormsg;
		}else
		{
			$this->dataarray['errormsg']='';
		}
	}
	public function index()
	{
		$this->load->view('users/login',$this->dataarray);
	}
}
