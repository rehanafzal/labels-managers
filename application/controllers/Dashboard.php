<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{
	public $content=array();
	public function __construct()
	{
		parent::__construct();
		$this->Common->Islogin();
		$this->content['mainmenu']=$this->uri->segment(1);
		$this->content['submenu']=$this->uri->segment(2);
		$erromsg=$this->session->flashdata('errormsg');
		$msgtype=$this->session->flashdata('msgtype');		
		if($erromsg!="")
		{
			$errormsg='<div class="row"><div class="col-sm-12"><div class="alert alert-'.$msgtype.'">'.$erromsg.'</div></div></div>';
			$this->content['errormsg']=$errormsg;
		}else
		{
			$this->content['errormsg']='';
		}
	}
	public function index()
	{
		$select='*,count(`tbl_labels_result`.`row_id`) as total,MAX(`founded_on`) as last_ex';
		$tbl='tbl_labels_result';
		$join=array();
		$join[]=array('tbl_website_list','tbl_labels_result.website_id =tbl_website_list.row_id','LEFT');
		$this->content['title']="Dashboard";
		$ordertype="DESC";	
		$orderby="tbl_labels_result.row_id,tbl_labels_result.founded_on";
	  //SELECT *,count(`tbl_labels_result`.`row_id`) as total FROM `tbl_labels_result` LEFT JOIN `tbl_website_list` ON `tbl_labels_result`.`website_id` =`tbl_website_list`.row_id GROUP BY `website_id`
		//$this->content['data']=$this->Common->GetAllRowJoin($tbl,"tbl_labels_result.row_id >0",$select,$join,$orderby,"",$ordertype,"website_id");
		$this->content['data']=$this->Common->GetAllRow('tbl_website_list',"row_id >0","*");
		//echo $this->db->last_query(); 		$this->Common->pre($this->content['data']); exit;
		//($tbl,$where="",$select="*",$join="",$orderby="",$numrow="",="",$groupby="")
		$this->load->view('users/view_dashboard',$this->content);
	}
}
