SELECT *, `tbl_labels_result`.`founded_on` AS `result_found` 
FROM `tbl_labels_result` 
LEFT JOIN `tbl_labels_list` 
ON `tbl_labels_result`.`label_id` = `tbl_labels_list`.`row_id` 
LEFT JOIN `tbl_website_list` 
ON `tbl_labels_result`.`website_id` = `tbl_website_list`.`row_id` 
LEFT JOIN `tbl_labels_list` 
ON `tbl_labels_list`.`user_id` = `tbl_users`.`user_id` 
WHERE  `tbl_labels_result`.`row_id` > 0 
ORDER  BY `tbl_labels_result`.`founded_on` ASC