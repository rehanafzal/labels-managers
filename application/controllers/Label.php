<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Label extends CI_Controller 
{
	public $content=array();
	public function __construct()
	{
		parent::__construct();
		$this->Common->Islogin();
		$erromsg=$this->session->flashdata('errormsg');
		$msgtype=$this->session->flashdata('msgtype');
		$this->content['mainmenu']=$this->uri->segment(1);
		$this->content['submenu']=$this->uri->segment(2);
		if($erromsg!="")
		{
			$errormsg='<div class="row"><div class="col-sm-12"><div class="alert alert-'.$msgtype.'">'.$erromsg.'</div></div></div>';
			$this->content['errormsg']=$errormsg;
		}else
		{
			$this->content['errormsg']='';
		}
	}		
	public function index()
	{
		$this->content['title']="Labels list";
		$tbl='tbl_labels_list';
		$where='status =1 OR status =0';
		$select="*";
		$orderby="row_id";
		$ordertype="DESC";
		$join=array();
		$join[]=array('tbl_users','tbl_labels_list.user_id = tbl_users.user_id','LEFT');
		$this->content['data']=$this->Common->GetAllRowJoin($tbl,$where,$select,$join,$orderby,"",$ordertype,"");
		$this->load->view('label/label_list',$this->content);
	}
	public function import()
	{
		$type=$this->input->post('add');		
		if($type=="file")
			{
				$truncate=$this->input->post('truncate');
				if($truncate!="")
				{
					$this->Common->truncate('tbl_labels_list');
					$this->Common->truncate('tbl_labels_result');
					sleep(1);
				}
				$config['upload_path']          = './upload/';
                $config['allowed_types']        = 'csv';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload('import'))
                {
                        $error = array('error' => $this->upload->display_errors());
						$this->session->set_flashdata('errormsg','<div class="alert alert-danger"><b>'.$error['error'].'</b></div>');
						$this->session->set_flashdata('msgtype', 'danger');
						redirect(site_url('label/import'));
						exit;
				}
                else
                {
                        $data = $this->upload->data();
						$row=0;
						if (($handle = fopen('./upload/'.$data['file_name'], "r")) !== FALSE) 
						{
							while (($csvdata = fgetcsv($handle, 1000, ",")) !== FALSE)
							{
								$DataArray					=	array();								
								$DataArray['label_name'] 	=	$csvdata[0];
								if($this->Common->GetSingleRow('tbl_labels_list',$DataArray,"row_id","row_id",0)<1)
								{
									$DataArray['insert_on']		=	date("Y-m-d h:s:i");
									$DataArray['user_id']		=	$this->Common->GetSessionIndex('user_id');								
									$this->Common->Insert('tbl_labels_list',$DataArray);								
									$row++;
								}
							}
						}						
                        $this->session->set_flashdata('message','<div class="alert alert-success"><b>('.$row.') record(s) has been import successfully.</b></div>');
						$this->session->set_flashdata('msgtype', 'success');
						redirect(site_url('label'));
						exit;
				}
			}
		$this->content['title']="Import Label list";
		$this->load->view('label/import',$this->content);
	}
	public function export($type)
	{
		$tbl="tbl_labels_list";
		$where=array('status'=>$type);
		$res=$this->Common->GetAllRow($tbl,$where,"label_name,found_on_url","founded_on","","DESC","");
		if($type==1)
		{
			$tbl='tbl_labels_result';
			$where='tbl_labels_result.row_id >0';
			$select="label_name,position,tbl_labels_result.found_on_url as result_found_url";
			$orderby="tbl_labels_result.founded_on";
			$ordertype="ASC";
			$join=array();
			$join[]=array('tbl_labels_list','tbl_labels_result.label_id = tbl_labels_list.row_id','LEFT');
			$join[]=array('tbl_website_list','tbl_labels_result.website_id = tbl_website_list.row_id','LEFT');
			$join[]=array('tbl_users','tbl_labels_list.user_id = `tbl_users`.`user_id`','LEFT');
			$res=$this->Common->GetAllRowJoin($tbl,$where,$select,$join,$orderby,"",$ordertype,"");
		}
		//$this->Common->pre($res); exit;
		$fp = fopen('./upload/label-list.csv', 'w');
		fputcsv($fp, array('Label Name','Position','URL'));
		foreach ($res as $fields) {
			fputcsv($fp, $fields);
		}
		fclose($fp);
		redirect(base_url('upload/label-list.csv'));
		exit;
	}
	public function founded()
	{
		$this->content['title']="Labels list";
		$tbl='tbl_labels_result';
		$where='tbl_labels_result.row_id >0 AND label_name!=""';
		$select="*,tbl_labels_result.founded_on as result_found,tbl_labels_result.found_on_url as result_found_url";
		$orderby="tbl_labels_result.founded_on";
		$ordertype="ASC";
		$join=array();
		$join[]=array('tbl_labels_list','tbl_labels_result.label_id = tbl_labels_list.row_id','LEFT');
		$join[]=array('tbl_website_list','tbl_labels_result.website_id = tbl_website_list.row_id','LEFT');
		$join[]=array('tbl_users','tbl_labels_list.user_id = `tbl_users`.`user_id`','LEFT');
		$this->content['data']=$this->Common->GetAllRowJoin($tbl,$where,$select,$join,$orderby,"",$ordertype,"");
		 //\\echo $this->db->last_query(); exit;
		//$this->Common->pre($this->content['data']); exit;
		//$this->content['data']=$this->Common->GetAllRow('tbl_labels_list',"","*","status,founded_on","","DESC","");		
		$this->load->view('label/label_found_list',$this->content);
	}
	public function delete($row_id)
	{
		$this->Common->Delrow('tbl_labels_list',array('row_id'=>$row_id));
		$this->Common->Delrow('tbl_labels_result',array('label_id'=>$row_id));
		$this->session->set_flashdata('errormsg', 'Label Deleted has been updated successfully!');
		$this->session->set_flashdata('msgtype', 'success');
		redirect(site_url('label'),"refresh");
		exit;
	}
	public function changestatus($row_id)
	{
		 $status=$this->uri->segment(4);
		 $daatarray=array();
		 $daatarray['status']=$status;
		 $daatarray['user_id']=$this->Common->GetSessionIndex('user_id');
		 $this->Common->Update('tbl_labels_list',$daatarray,array('row_id'=>$row_id));			
		 $this->session->set_flashdata('errormsg', 'Label <b>STATUS</b> has been updated successfully!');
		 $this->session->set_flashdata('msgtype', 'success');
		 redirect(site_url('label'),"refresh");
		 exit;	
	}
	public function edit($row_id)
	{
		$daatarray=array();
		$add=$this->input->post('isubmit');
		if($add==1)
		{
			 $daatarray['label_name']=$this->input->post('account_name');			  
			 $this->Common->Update('tbl_labels_list',$daatarray,array('row_id'=>$row_id));
			 $daatarray['user_id']=$this->Common->GetSessionIndex('user_id');			 
			 $this->session->set_flashdata('errormsg', 'Label values has been updated successfully!');
  			 $this->session->set_flashdata('msgtype', 'success');
			 redirect(site_url('label'),"refresh");
			 exit;
		}			
		$this->content['data']=$this->Common->GetSingleRow('tbl_labels_list',array('row_id'=>$row_id),"*","",0);
		$this->content['title']="Edit Label";		
		$this->load->view('label/add',$this->content);
	}
	public function add()
	{
		$add=$this->input->post('isubmit');
		if($add==1)
		{
			 $label=explode(";",$this->input->post('account_name'));	
			 if(count($label)<0)
			 {
				 $this->session->set_flashdata('errormsg', 'label Name is required!');
				 $this->session->set_flashdata('msgtype', 'danger');
				 redirect(site_url('label'));
				 exit;
			 }
			 $labelc=0;
			 for($j=0;$j<count($label);$j++)
			 {
					$daatarray=array();					
					$daatarray['insert_on']=date("Y-m-d h:s:i"); //2017-02-14 10:51:33
					if($this->Common->GetSingleRow('tbl_labels_list',$daatarray,"row_id","row_id",0)<1)
					{
						$daatarray['user_id']=$this->Common->GetSessionIndex('user_id');
						$daatarray['label_name']=$this->Common->removeWhiteSpace($label[$j]);
						$this->Common->Insert('tbl_labels_list',$daatarray);
						$labelc++;
					}
			 }
			 $this->session->set_flashdata('errormsg', '$labelc New Label(s) has been added successfully!');
  			 $this->session->set_flashdata('msgtype', 'success');
			 redirect(site_url('label'),"refresh");
			 exit;
		}			
		$this->content['title']="Add Label";		
		$this->load->view('label/add',$this->content);
	}
}