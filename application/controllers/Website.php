<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends CI_Controller 
{
	public $content=array();
	public function __construct()
	{
		parent::__construct();
		$this->Common->Islogin();
		$erromsg=$this->session->flashdata('errormsg');
		$msgtype=$this->session->flashdata('msgtype');
		$this->content['mainmenu']=$this->uri->segment(1);
		$this->content['submenu']=$this->uri->segment(2);
		if($erromsg!="")
		{
			$errormsg='<div class="row"><div class="col-sm-12"><div class="alert alert-'.$msgtype.'">'.$erromsg.'</div></div></div>';
			$this->content['errormsg']=$errormsg;
		}else
		{
			$this->content['errormsg']='';
		}
	}		
	public function index()
	{
		$this->content['title']="Website list";
		$this->content['data']=$this->Common->GetAllRow('tbl_website_list',"website_name !=''","*","row_id","","DESC","");		
		//GetAllRow($tbl,$where="",$select="*",$orderby="",$numrow="",$ordertype="",$limit="")
		$this->load->view('website/website_list',$this->content);
	}
	public function delete($row_id)
	{
		$this->Common->Delrow('tbl_website_list',array('row_id'=>$row_id));
		$this->session->set_flashdata('errormsg', 'Website Deleted has been updated successfully!');
		$this->session->set_flashdata('msgtype', 'success');
		redirect(site_url('website'),"refresh");
		exit;
	}		
	public function edit($row_id)
	{
		$daatarray=array();
		$add=$this->input->post('isubmit');
		if($add==1)
		{
			 $daatarray['website_name']=$this->input->post('account_name');			  
			 $daatarray['script_file_name']=$this->input->post('script_file_name');			  
			 $daatarray['website_url']=$this->input->post('account_url');
			$daatarray['run_auto']=$this->input->post('auto_run');			
			if($daatarray['run_auto']=="")
			{
				$daatarray['run_auto']=0;
			}			 
			 $this->Common->Update('tbl_website_list',$daatarray,array('row_id'=>$row_id));			
			 $this->session->set_flashdata('errormsg', 'Website values has been updated successfully!');
  			 $this->session->set_flashdata('msgtype', 'success');
			 redirect(site_url('website'),"refresh");
			 exit;
		}			
		$this->content['data']=$this->Common->GetSingleRow('tbl_website_list',array('row_id'=>$row_id),"*","",0);
		$this->content['title']="Edit Website";		
		$this->load->view('website/add',$this->content);
	}
	public function add()
	{
		$add=$this->input->post('isubmit');
		if($add==1)
		{
			 $label=explode(";",$this->input->post('account_name'));	
			 if(count($label)<0)
			 {
				 $this->session->set_flashdata('errormsg', 'website Name is required!');
				 $this->session->set_flashdata('msgtype', 'danger');
				 redirect(site_url('website'));
				 exit;
			 }			
			$daatarray=array();					
			$daatarray['website_name']=$this->input->post('account_name');			  
			$daatarray['script_file_name']=$this->input->post('script_file_name');			  
			$daatarray['website_url']=$this->input->post('account_url');
			$daatarray['run_auto']=$this->input->post('auto_run');			
			if($daatarray['run_auto']=="")
			{
				$daatarray['run_auto']=0;
			}
			if($this->Common->GetSingleRow('tbl_website_list',$daatarray,"row_id","row_id",0)<1)
			{
				//$daatarray['website_name']=$label[$j];
				$this->Common->Insert('tbl_website_list',$daatarray);				
			}			 
			 $this->session->set_flashdata('errormsg', 'New Website has been added successfully!');
  			 $this->session->set_flashdata('msgtype', 'success');
			 redirect(site_url('website'),"refresh");
			 exit;
		}			
		$this->content['title']="Add New Website";		
		$this->load->view('website/add',$this->content);
	}
}