<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 
{
	public $content=array();
	public function __construct()
	{
		parent::__construct();
		$this->Common->Islogin();
		if($this->Common->GetSessionIndex('user_type')!=1)
		{
			$this->session->set_flashdata('errormsg', 'You have no access of this area!');
			$this->session->set_flashdata('msgtype', 'danger');
			redirect(site_url('dashboard'),"refresh");
			exit;
		}
		$erromsg=$this->session->flashdata('errormsg');
		$msgtype=$this->session->flashdata('msgtype');
		$this->content['mainmenu']=$this->uri->segment(1);
		$this->content['submenu']=$this->uri->segment(2);
		$this->content['websites']=$this->Common->GetAllRow('tbl_user_type','row_id >0',"*","row_id","","DESC","");
		if($erromsg!="")
		{
			$errormsg='<div class="row"><div class="col-sm-12"><div class="alert alert-'.$msgtype.'">'.$erromsg.'</div></div></div>';
			$this->content['errormsg']=$errormsg;
		}else
		{
			$this->content['errormsg']='';
		}
	}	 
	public function index()
	{
		$this->content['title']="Users List";
		$this->content['data']=$this->Common->GetAllRow('tbl_users',"user_name !=''","*","user_update_on","","DESC","");		
		//GetAllRow($tbl,$where="",$select="*",$orderby="",$numrow="",$ordertype="",$limit="")
		$this->load->view('user/user_list',$this->content);
	}
	public function delete($row_id)
	{
		$this->Common->Delrow('tbl_users',array('user_id'=>$row_id));
		$this->session->set_flashdata('errormsg', 'URL Deleted has been updated successfully!');
		$this->session->set_flashdata('msgtype', 'success');
		redirect(site_url('user'),"refresh");
		exit;
	}		
	public function edit($row_id)
	{
		$daatarray=array();
		$add=$this->input->post('isubmit');
		if($add==1)
		{
			 $account_name		=	$this->input->post('account_name');	
			 $account_password	=	$this->input->post('account_password');
			 $account_type		=	$this->input->post('account_type');
			 $daatarray['user_name']	= $account_name;
			 if($account_password!="")
			 {
				$daatarray['user_password']= md5($account_password);
			 }
			 if($this->Common->GetSingleRow('tbl_users',$daatarray,"user_id","user_id",0)<1)
			 {
				$this->Common->Update('tbl_users',$daatarray,array('user_id'=>$row_id));
				$this->session->set_flashdata('errormsg', 'User has been updated successfully!');				
				$this->session->set_flashdata('msgtype', 'success');
				redirect(site_url('user'),"refresh");
				exit;
			 }else
			 {
				$this->session->set_flashdata('errormsg', '<b style="font-family:verdana;">Something wrong with your informaiton,Please try with different UserName and Password.</b>');
				$this->session->set_flashdata('msgtype', 'danger');
				redirect(site_url('user/edit/'.$row_id),"refresh");
				exit;
			 }
			 
		}			
		$this->content['data']=$this->Common->GetSingleRow('tbl_users',array('user_id'=>$row_id),"*","",0);
		$this->content['title']="Edit User";		
		$this->load->view('user/add',$this->content);
	}
	public function add()
	{
		$add=$this->input->post('isubmit');
		if($add==1)
		{
			 $account_name		=	$this->input->post('account_name');	
			 $account_password	=	$this->input->post('account_password');
			 $account_type		=	$this->input->post('account_type');
			 if($account_name=="" || $account_password=="")
			 {
				 $this->session->set_flashdata('errormsg', 'Invalid User info');
				 $this->session->set_flashdata('msgtype', 'danger');
				 redirect(site_url('url'));
				 exit;
			 }
			 $labelc=0;
			 $daatarray['user_name']	= $account_name;
			 $daatarray['user_password']= md5($account_password);			
			 if($this->Common->GetSingleRow('tbl_users',$daatarray,"user_id","user_id",0)<1)
				{
					$daatarray['user_type']	= $account_type;
					$this->Common->Insert('tbl_users',$daatarray);
					$labelc++;
				}else
				{
					$this->session->set_flashdata('errormsg','User Already Exist!');
					$this->session->set_flashdata('msgtype', 'danger');
					redirect(site_url('user/add'),"refresh");
					exit;
				}
			 $this->session->set_flashdata('errormsg', 'New User has been added successfully!');
  			 $this->session->set_flashdata('msgtype', 'success');
			 redirect(site_url('user'),"refresh");
			 exit;
		}			
		$this->content['title']="Add User";		
		$this->load->view('user/add',$this->content);
	}
}