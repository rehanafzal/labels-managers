SELECT *, Count(`tbl_labels_result`.`row_id`) AS total
FROM   `tbl_labels_result` 
LEFT JOIN `tbl_website_list` 
ON `tbl_labels_result`.`website_id` = `tbl_website_list`.`row_id` 
WHERE  `tbl_labels_result`.`row_id` > 0
GROUP  BY `website_id` 
ORDER  BY `tbl_labels_result`.`row_id` DESC