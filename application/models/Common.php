<?php
class Common extends CI_Model 
{
	
	public function removeWhiteSpace($text) // Remove White Space
	{
		 $text = preg_replace('/[\t\n\r\0\x0B]/', ' ', $text);
		 $text = preg_replace('/([\s])\1+/', ' ', $text);
		 $text = trim($text);
		 return $text;
	}
	public function Islogin()
	{
		if(!$this->GetSessionIndex('user_id') || $this->GetSessionIndex('user_id')== "")
		{			
			redirect(site_url("login"),"refresh");
			exit;
		}
	}
	function truncate($table)
	{
		return $this->db->truncate($table);
	}
	function Delrow($table,$where)
	{
		$this->db->where($where);
		return $this->db->delete($table);		
	}
	function Update($table,$dataarry,$where)
	{
		$this->db->where($where);
		return $this->db->update($table,$dataarry);		
	}
	function Insert($table,$dataarry)
	{
		$this->db->insert($table,$dataarry);
		return $this->db->insert_id();
	}
	function dbdate($date)
	{
		return date("Y-m-d",strtotime($date));
	}
	function webdate($date)
	{
		return date("m/d/Y",strtotime($date));
	}
	function pre($arr)
	{
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
	function GetSessionIndex($index)
	{
		return $this->session->userdata($index);
	}
	function GetAllRowJoin($tbl,$where="",$select="*",$join="",$orderby="",$numrow="",$ordertype="",$groupby="")
	{
		$this->db->select($select);
		$this->db->from($tbl);
		foreach($join as $j):
			$this->db->join($j[0],$j[1],$j[2]);
		endforeach;
		//exit;
		$this->db->where($where);
		$this->db->group_by($groupby);		
		$this->db->order_by($orderby,$ordertype);
		
		$res=$this->db->get();		
		if($numrow==1)
		{
			return $res->num_rows();
		}else
		{	
			return $res->result_array();
		}
		
	}
	function GetAllRowWithLimit($tbl,$where="",$select="*",$orderby="",$numrow="",$ordertype="",$Startlimit="",$limit=10)
	{
		$this->db->select($select);
		$this->db->from($tbl);
		$this->db->where($where);
		$this->db->order_by($orderby,$ordertype);
		$this->db->limit($limit, $Startlimit);
		$res=$this->db->get();
		if($numrow==1)
		{
			return $res->num_rows();
		}else
		{	
			return $res->result_array();
		}
	}
	function GetAllRow($tbl,$where="",$select="*",$orderby="",$numrow="",$ordertype="",$limit="")
	{
		$this->db->select($select);
		$this->db->from($tbl);
		$this->db->where($where);
		$this->db->order_by($orderby,$ordertype);
		$this->db->limit($limit);
		$res=$this->db->get();
		//ECHO $this->db->last_query(); exit;
		if($numrow==1)
		{
			return $res->num_rows();
		}else
		{	
			return $res->result_array();
		}
	}
	function GetSingleRow($tbl,$where="",$select="*",$orderby="",$numrow="",$order_type="ASC")
	{
		$this->db->select($select);
		$this->db->from($tbl);
		$this->db->where($where);
		$this->db->order_by($orderby,$order_type);
		$res=$this->db->get();
		if($numrow==1)
		{
			return $res->num_rows();
		}else
		{	
			return $res->row_array();
		}
	}	
}
