<?php $this->load->view('include/header'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<style type="text/css">
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
</head>
<body class="sticky-header">
<section>
    <?php $this->load->view('include/menu'); ?>
    <!-- main content start-->
    <div class="main-content">
		<?php $this->load->view('include/topnotifications'); ?>	
        <!--body wrapper start-->
        <div class="wrapper">
              <div class="rows">
				<div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed" id="myTable">
                        <thead>
                        <tr>
                            <th>WEBSITE NAME</th>                            
                            <th>WEBSITE URL</th>
                            <th>UPDATED DATE</th>                                                     
                            <th>ACTION</th>                                                     
                        </tr>
                        </thead>
						<tbody>
						<?php foreach($data as $row): ?>	
							<tr>
								<td><?php echo $row['website_name']; ?></td>
								<td><?php echo $row['website_url']; ?></td>
								<td><?php echo $row['update_date']; ?></td>
								<td>
								<a href="<?php echo site_url('website/edit')."/".$row['row_id'];  ?>"><i class="fa fa-pencil">&nbsp;</i></a>
								<a href="<?php echo site_url('website/delete')."/".$row['row_id'];  ?>" onClick="return confirm('Are you sure want to delete this?')" ><i class="fa fa-trash-o">&nbsp;</i></a>
								</td>
							</tr>
						<?php endforeach; ?>	
						</tbody>
						<tfoot>
						<tr>
							<th>WEBSITE NAME</th>                            
                            <th>WEBSITE URL</th>
                            <th>UPDATED DATE</th>                                                     
                            <th>ACTION</th> 
						</tr>
					   </tfoot>                        
                    </table>
                </section>
            </div>
			  </div>	
        </div>
        <footer>
            <?php echo date("Y");?> &copy; Developed by <strong>The Website Guy.</strong>
        </footer>
     </div>   
</section>
<?php $this->load->view('include/footer'); ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#myTable').DataTable({"order": [[ 3, "desc" ]]});
});
/*$(document).ready(function() {
    $('#myTable').DataTable( {
        "processing": true,
        "serverSide": true,
		"pageLength": 50,
		"aLengthMenu": [[50, -1], [50, "All"]],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } ); 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },    
        "ajax": {
            "url": "<?php echo site_url('label/GetStockAPI/'); ?>",
            "type": "POST"
        },
        "columns": [
            { "data": "label_name" },
            { "data": "found_on_url" },
            { "data": "founded_on" },
            { "data": "insert_on" }
        ]
    } );
} ); */
/*$(document).ready(function() {
    $('#myTable').DataTable( {
		"aLengthMenu": [[50, -1], [50, "All"]],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } ); 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
	$('#myTable tfoot tr').insertAfter($('#example thead tr')); 
} );  */
</script>