<?php $this->load->view('include/header'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<style type="text/css">
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
</head>
<body class="sticky-header">
<section>
    <?php $this->load->view('include/menu'); ?>
    <!-- main content start-->
    <div class="main-content">
		<?php $this->load->view('include/topnotifications'); ?>	
        <!--body wrapper start-->
        <div class="wrapper">
              <div class="rows">
				&nbsp;<a href="<?php echo site_url('stock/download'); ?>" class="btn btn-success"><i class="fa fa-download"></i>&nbsp;Download Stock CSV</a>
				&nbsp;<a href="<?php echo site_url('stock/emptystock'); ?>" onClick="return confirm('Are you sure want to empty stock?')" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Empty Stock</a>
				<div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed" id="myTable">
                        <thead>
                        <tr>
                            <th>Product Name</th>                            
                            <th>Product Size</th>
                            <th>Stock QTY</th>                                                     
                            <th>Update ON</th>                                                     
                        </tr>
                        </thead>
						<tfoot>
						<tr>
							<th>Product Name</th>                            
                            <th>Product Size</th>
                            <th>Stock QTY</th>                                                     
                            <th>Update ON</th> 
						</tr>
					   </tfoot>                        
                    </table>
                </section>
            </div>
			  </div>	
        </div>
        <footer>
            <?php echo date("Y");?> &copy; Developed by <strong>The Website Guy.</strong>
        </footer>
     </div>   
</section>
<?php $this->load->view('include/footer'); ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#myTable').DataTable( {
        "processing": true,
        "serverSide": true,
		"pageLength": 50,
		"aLengthMenu": [[50, -1], [50, "All"]],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } ); 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },    
        "ajax": {
            "url": "<?php echo site_url('stock/GetStockAPI/'); ?>",
            "type": "POST"
        },
        "columns": [
            { "data": "product_name" },
            { "data": "product_size" },
            { "data": "product_stock_qty" },
            { "data": "product_stock_update_on" }
        ]
    } );
} );
/*$(document).ready(function() {
    $('#myTable').DataTable( {
		"aLengthMenu": [[50, -1], [50, "All"]],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } ); 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
	$('#myTable tfoot tr').insertAfter($('#example thead tr')); 
} );  */
</script>