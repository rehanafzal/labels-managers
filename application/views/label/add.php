<?php $this->load->view('include/header'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/bootstrap-datepicker/css/datepicker-custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/bootstrap-timepicker/css/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />
</head>
<body class="sticky-header">
<section>
    <?php $this->load->view('include/menu'); ?>
    <!-- main content start-->
    <div class="main-content" >
		<?php $this->load->view('include/topnotifications'); ?>			
        <!--body wrapper start-->
		<div class="wrapper">
              <div class="rows">
			  <a href="<?php echo site_url('label/import'); ?>" class="btn-sm btn-info"><i class="fa fa-upload">&nbsp;&nbsp;</i>Import</a>
        
			  <?php if($this->uri->segment(2)=="add") { ?>
				<form class="form-horizontal" method="post" action="<?php echo current_url(); ?>" >
				  <input type="hidden" name="isubmit" value="1">			
				  <div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label"><b>Enter Label Name :</b></label>
					<div class="col-sm-8">
					  <input type="text" class="form-control" name="account_name" placeholder="Enter Label Name and multiple with (;) separator." required>
					</div>
				  </div>
				  <div class="form-group">
					<div class="col-sm-offset-3 col-sm-10">
					  <button type="submit" class="btn btn-success"><i class="fa fa-check">&nbsp;</i>Save</button>
					  <a href="<?php echo site_url('user'); ?>" class="btn btn-danger"><i class="fa fa-times">&nbsp;</i>Cancel</a>
					</div>
				  </div>
				</form>
			  <?php } else { ?>
				<form class="form-horizontal" method="post" action="<?php echo current_url(); ?>" >
				  <input type="hidden" name="isubmit" value="1">			
				  <div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label"><b>URL  :</b></label>
					<div class="col-sm-6">
					  <input type="text" class="form-control" name="account_name" value="<?php echo $data['label_name']; ?>" placeholder="Account Name" required>
					</div>
				  </div>				   
				  <div class="form-group">
					<div class="col-sm-offset-3 col-sm-10">
					  <button type="submit" class="btn btn-success"><i class="fa fa-check">&nbsp;</i>Update</button>
					  <a href="<?php echo site_url('user'); ?>" class="btn btn-danger"><i class="fa fa-times">&nbsp;</i>Cancel</a>
					</div>
				  </div>
				</form>
			  <?php } ?>
			  </div>	
        </div>
        <footer>
            <?php echo date("Y");?> &copy; Developed by <strong>The Website Guy.</strong>
        </footer>
     </div>   
</section>
<?php $this->load->view('include/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>js/pickers-init.js"></script>
