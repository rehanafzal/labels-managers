<?php $this->load->view('include/header'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<style type="text/css">
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
</head>
<body class="sticky-header">
<section>
    <?php $this->load->view('include/menu'); ?>
    <!-- main content start-->
    <div class="main-content">
		<?php $this->load->view('include/topnotifications'); ?>	
        <!--body wrapper start-->
        <div class="wrapper">
		<a href="<?php echo site_url('label/export/0'); ?>" class="btn-sm btn-success"><i class="fa fa-download">&nbsp;&nbsp;</i>Export</a>
		<a href="<?php echo site_url('label/import'); ?>" class="btn-sm btn-info"><i class="fa fa-upload">&nbsp;&nbsp;</i>Import</a>
              <div class="rows">
				<div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed" id="myTable">
                        <thead>
                        <tr>
                            <th>LABLE NAME</th>                            
                            <th>LABLE URL</th>
                            <th>FOUNDED ON</th>                                                   
                            <th>STATUS</th>                                                   
                            <th>ACTION</th>                                                     
                        </tr>
                        </thead>
						<tbody>
						<?php foreach($data as $row): ?>	
							<tr>
								<td><?php echo $row['label_name']; ?></td>
								<td>
								<?php
									if($row['found_on_url']!="")
									{
										?>
										<a class="btn-sm btn-success" title="<?php echo $row['found_on_url']; ?>" href="<?php echo $row['found_on_url']; ?>" target="_blank"><i class="fa fa-external-link">&nbsp;</i>Click Here</a>
										<?php
									}
								?>
								</td>
								<td><?php echo $row['founded_on']; ?></td>
								<td>
								<?php if($row['status']==1) { ?>	
									<a href="<?php echo site_url('label/changestatus')."/".$row['row_id']."/0"; ?>"><span class="btn-sm btn-success"><i class="fa fa-check">&nbsp;</i>Managed By <b class="text-danger"><?php echo $row['user_name']; ?></b></span></a>
								<?php } else { ?>
									<a href="<?php echo site_url('label/changestatus')."/".$row['row_id']."/1"; ?>"><span class="btn-sm btn-danger"><i class="fa fa-refresh">&nbsp;</i>Waiting</span></a>
								<?php } ?>
								</td>
								<td>
								<a href="<?php echo site_url('label/edit')."/".$row['row_id'];  ?>"><i class="fa fa-pencil">&nbsp;</i></a>
								<a href="<?php echo site_url('label/delete')."/".$row['row_id'];  ?>" onClick="return confirm('Are you sure want to delete this?')" ><i class="fa fa-trash-o">&nbsp;</i></a>
								</td>
							</tr>
						<?php endforeach; ?>	
						</tbody>
						<tfoot>
						<tr>
							<th>LABLE NAME</th>                            
                            <th>LABLE URL</th>
                            <th>FOUNDED ON</th>                                                     
                            <th>STATUS</th>                                                     
                            <th>ACTION</th> 
						</tr>
					   </tfoot>                        
                    </table>
                </section>
            </div>
		</div>
        </div>
        <footer>
            <?php echo date("Y");?> &copy; Developed by <strong>The Website Guy.</strong>
        </footer>
     </div>   
</section>
<?php $this->load->view('include/footer'); ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#myTable').DataTable({"order": [[ 2, "desc" ]]});
});
</script>