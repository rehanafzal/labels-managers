<?php $this->load->view('include/header'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/bootstrap-datepicker/css/datepicker-custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/bootstrap-timepicker/css/timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />
</head>
<body class="sticky-header">
<section>
    <?php $this->load->view('include/menu'); ?>
    <!-- main content start-->
    <div class="main-content" >
		<?php $this->load->view('include/topnotifications'); ?>	
        <!--body wrapper start-->
		<div class="wrapper">
              <div class="rows">			  
				<form onsubmit="return confi();" enctype="multipart/form-data" class="form-horizontal" method="post" action="<?php echo current_url(); ?>" >
				  <input type="hidden" name="isubmit" value="1">			
				  <input type="hidden" name="add" value="file">
				  <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Choose File</label>
					<div class="col-sm-4">
					  <input type="file" name="import" class="form-control" id="inputEmail3" required>
					</div>
				  </div>
				 <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">TRUNCATE labels</label>
					<div class="col-sm-1">
					  <input type="checkbox" name="truncate" id="truncate" class="form-control">
					</div>
				  </div>				  
				  <div class="form-group">
					<div class="col-sm-offset-2 col-sm-8">
					  <button type="submit" class="btn btn-success"><i class="fa fa-plus">&nbsp;</i>Import</button>					 
					</div>
				  </div>
				</form> 
			  </div>	
        </div>
        <footer>
            <?php echo date("Y");?> &copy; Developed by <strong>The Website Guy.</strong>
        </footer>
     </div>   
</section>
<?php $this->load->view('include/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>js/pickers-init.js"></script>
<script type="text/javascript">
function confi()
{
	if($("#truncate").is(':checked'))
	{
		return confirm('Are you sure want to TRUNCATE existing lables?');
	}
	return true;
}
</script>