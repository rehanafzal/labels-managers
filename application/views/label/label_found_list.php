<?php $this->load->view('include/header'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<style type="text/css">
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
</head>
<body class="sticky-header">
<section>
    <?php $this->load->view('include/menu'); ?>
    <!-- main content start-->
    <div class="main-content">
		<?php $this->load->view('include/topnotifications'); ?>	
        <!--body wrapper start-->
        <div class="wrapper">
              <div class="rows">
				<a href="<?php echo site_url('label/export/1'); ?>" class="btn-sm btn-success"><i class="fa fa-download">&nbsp;&nbsp;</i>Export</a>
				<div class="panel-body">
                <section id="unseen">
                    <table class="table table-bordered table-striped table-condensed" id="myTable">
                        <thead>
                        <tr>
                            <th>LABLE NAME</th>                            
                            <th>LABLE URL</th>
                            <th>POSITION</th>                                                   
                            <th>FOUNDED ON</th>                                                   
                            <th>WEBSITE</th>                                                   
                            <th>STATUS</th>                                                                                                                               
                        </tr>
                        </thead>
						<tbody>
						<?php foreach($data as $row): ?>	
							<tr>
								<td><?php echo $row['label_name']; ?></td>
								<td>
								<?php
									if($row['result_found_url']!="")
									{
										?>
										<a class="btn-sm btn-success" title="<?php echo $row['result_found_url']; ?>" href="<?php echo $row['result_found_url']; ?>" target="_blank"><i class="fa fa-external-link">&nbsp;</i>Click Here</a>
										<?php
									}
								?>
								</td>
								<td><?php echo $row['position']; ?></td>
								<td><?php echo $row['result_found']; ?></td>
								<td><?php echo $row['website_name']; ?></td>
								<td>
								<?php if($row['status']==1) { ?>	
									<a href="<?php echo site_url('label/changestatus')."/".$row['label_id']."/0"; ?>"><span class="btn-sm btn-success"><i class="fa fa-check">&nbsp;</i>Managed By <b class="text-danger"><?php echo $row['user_name']; ?></b></span></a>
								<?php } else { ?>
									<a href="<?php echo site_url('label/changestatus')."/".$row['label_id']."/1"; ?>"><span class="btn-sm btn-danger"><i class="fa fa-refresh">&nbsp;</i>Waiting</span></a>
								<?php } ?>
								</td>								 
							</tr>
						<?php endforeach; ?>	
						</tbody>
						<tfoot>
						<tr>
							<th>LABLE NAME</th>                            
                            <th>LABLE URL</th>
                            <th>FOUNDED ON</th>
							<th>WEBSITE</th> 							
                            <th>STATUS</th>                                                                                 
						</tr>
					   </tfoot>                        
                    </table>
                </section>
            </div>
		</div>
        </div>
        <footer>
            <?php echo date("Y");?> &copy; Developed by <strong>The Website Guy.</strong>
        </footer>
     </div>   
</section>
<?php $this->load->view('include/footer'); ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#myTable').DataTable({
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
		});
});
</script>