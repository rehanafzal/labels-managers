
<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/modernizr.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.nicescroll.js"></script>

<script src="<?php echo base_url(); ?>js/iCheck/jquery.icheck.js"></script>
<script src="<?php echo base_url(); ?>js/icheck-init.js"></script>


<!--common scripts for all pages-->
<script src="<?php echo base_url(); ?>js/scripts.js"></script>


</body>
</html>