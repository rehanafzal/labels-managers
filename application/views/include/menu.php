<!-- left side start-->
<div class="left-side sticky-left-side">

	<!--logo and iconic logo start-->
	<div class="logo">
		<a href="/"><img src="<?php echo base_url(); ?>images/logo.png" alt=""></a>
	</div>

	<div class="logo-icon text-center">
		<a href="/"><img src="<?php echo base_url(); ?>images/logo_icon.png" alt=""></a>
	</div>
	<!--logo and iconic logo end-->

	<div class="left-side-inner">
		<!-- visible to small devices only -->
		<div class="visible-xs hidden-sm hidden-md hidden-lg">
			<div class="media logged-user">
				<!--img alt="" src="<?php echo base_url(); ?>images/photos/user-avatar.png" class="media-object"-->
				<div class="media-body">
					<h4><a href="#">Ego</a></h4>
					<span>"Hello There..."</span>
				</div>
			</div>
			<h5 class="left-nav-title">Account Information</h5>
			<ul class="nav nav-pills nav-stacked custom-nav">			  
			  <li><a href="#"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
			</ul>
		</div>
		<!--sidebar nav start-->
		<ul class="nav nav-pills nav-stacked custom-nav">
			<li <?php if($mainmenu=="dashboard") { echo 'class="active"'; } ?>><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
			<li class="menu-list<?php if($mainmenu=="label") { echo " nav-active"; } ?>"><a href=""><i class="fa fa-laptop"></i> <span>Labels</span></a>
				<ul class="sub-menu-list">
					<li class="<?php if($submenu=="add" && $mainmenu=="label") { echo "active"; } ?>" ><a href="<?php echo site_url('label/add'); ?>">Add New Label</a></li>
					<li class="<?php if($submenu=="" && $mainmenu=="label") { echo "active"; } ?>"><a href="<?php echo site_url('label'); ?>">View All Labels</a></li>					
					<li class="<?php if($submenu=="founded" && $mainmenu=="label") { echo "active"; } ?>"><a href="<?php echo site_url('label/founded'); ?>">Features</a></li>					
				</ul>
			</li>
			<li class="menu-list<?php if($mainmenu=="website") { echo " nav-active"; } ?>"><a href=""><i class="fa fa-external-link"></i> <span>Websites</span></a>
				<ul class="sub-menu-list">
					<li class="<?php if($submenu=="add" && $mainmenu=="website") { echo "active"; } ?>" ><a href="<?php echo site_url('website/add'); ?>">Add New Website</a></li>
					<li class="<?php if($submenu=="" && $mainmenu=="website") { echo "active"; } ?>"><a href="<?php echo site_url('website'); ?>">View All Websites</a></li>					
				</ul>
			</li>
			<li class="menu-list<?php if($mainmenu=="url") { echo " nav-active"; } ?>"><a href=""><i class="fa fa-star"></i> <span>URLs</span></a>
				<ul class="sub-menu-list">
					<li class="<?php if($submenu=="add" && $mainmenu=="url") { echo "active"; } ?>" ><a href="<?php echo site_url('url/add'); ?>">Add New URLs</a></li>
					<li class="<?php if($submenu=="" && $mainmenu=="url") { echo "active"; } ?>"><a href="<?php echo site_url('url'); ?>">View All URLs</a></li>					
				</ul>
			</li>
			<?php if($this->Common->GetSessionIndex('user_type')==1) { ?>
			<li class="menu-list<?php if($mainmenu=="user") { echo " nav-active"; } ?>"><a href=""><i class="fa fa-users"></i> <span>Users</span></a>
				<ul class="sub-menu-list">
					<li class="<?php if($submenu=="add" && $mainmenu=="user") { echo "active"; } ?>" ><a href="<?php echo site_url('user/add'); ?>">Add New User</a></li>
					<li class="<?php if($submenu=="" && $mainmenu=="user") { echo "active"; } ?>"><a href="<?php echo site_url('user'); ?>">View All Users</a></li>					
				</ul>
			</li>
			<?php } ?>
			<li><a href="<?php echo site_url('auth/logout') ?>"><i class="fa fa-sign-out"></i> <span>Log out</span></a></li>
		</ul>
		<!--sidebar nav end-->

	</div>
</div>
