<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link rel="shortcut icon" href="#" type="image/png">
  <title><?php echo $title; ?></title>
  <!--icheck-->
  <link href="<?php echo base_url(); ?>js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>js/iCheck/skins/square/square.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>js/iCheck/skins/square/red.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>js/iCheck/skins/square/blue.css" rel="stylesheet">
  <!--dashboard calendar-->
  <link href="<?php echo base_url(); ?>css/clndr.css" rel="stylesheet">
  <!--common-->
  <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>css/style-responsive.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url(); ?>js/html5shiv.js"></script>
  <script src="<?php echo base_url(); ?>js/respond.min.js"></script>
  <![endif]-->
