
<div class="page-heading">
<?php if($errormsg!="" && isset($errormsg)) { echo $errormsg; } ?>
<h3><?php echo ucwords($this->uri->segment(1)); ?></h3>
<ul class="breadcrumb">
	<li>
		<a href="<?php echo site_url('dashboard'); ?>">Dashboard</a>
	</li>
	<li>
		<a href="<?php echo site_url($this->uri->segment(1)); ?>"><?php echo ucwords($this->uri->segment(1)); ?></a>
	</li>
	<li class="active"><?php echo ucwords($this->uri->segment(2)); ?></li>
</ul>
</div>