<?php $this->load->view('include/header'); ?>
<body class="sticky-header">
<section>
    <?php $this->load->view('include/menu'); ?>
    <!-- main content start-->
    <div class="main-content" >
		<?php $this->load->view('include/topnotifications'); ?>			
        <!--body wrapper start-->
        <div class="wrapper">
            <div class="row states-info">
            <?php 
			if(count($data)>0) {
			$col=array('red-bg','blue-bg','green-bg','yellow-bg');
			foreach($data as $row): 
			$select='*,count(`tbl_labels_result`.`row_id`) as total,MAX(`founded_on`) as last_ex';
			$tbl='tbl_labels_result';
			$join=array();
			$join[]=array('tbl_website_list','tbl_labels_result.website_id =tbl_website_list.row_id','LEFT');
			$this->content['title']="Dashboard";
			$ordertype="DESC";	
			$orderby="tbl_labels_result.row_id,tbl_labels_result.founded_on";
			$where="tbl_labels_result.row_id >0 AND tbl_website_list.row_id=".$row['row_id']."";
			$dd=$this->Common->GetAllRowJoin($tbl,$where,$select,$join,$orderby,"",$ordertype,"website_id");
			//$this->Common->pre($dd);exit;
			?>
			<div class="col-md-4" style="font-family:verdana;">
                <div class="panel red-bg">
                    <div class="panel-body">
                        <div class="row" >                            
                            <div class="col-xs-12">
                                <span class="state-title btn-sm btn-success"><b>Website : <?php echo $row['website_name']; ?></b></span>
                                <br/>
                                <br/>
								<b class="btn-sm btn-primary">Total Founded Label(s) : <?php if(count($dd)>0) { echo number_format($dd[0]['total'],1); } else { echo "0.00"; } ?> </b>
                                <br/>
                                <br/>
								<b class="btn-sm btn-info">Last Execution : <?php if(count($dd)>0) { echo $dd[0]['last_ex']; } ?></b>
								<br/>
                                <br/>
								<b class="btn-sm btn-danger"><a onclick="return confirm('Are you sure?')" href="<?php echo base_url('scraping-script'); ?>/<?php echo $row['script_file_name']; ?>" target="_blank">Run Script</a></b>
							</div>
                        </div>
                    </div>
                </div>
            </div>
			<?php endforeach; } else {?>
			<div class="col-md-4" style="font-family:verdana;">
                <div class="panel red-bg">
                    <div class="panel-body">
                        <div class="row">
							<div class="col-xs-12">
								No Result Found...
							</div>
						</div>
					</div>
				</div>
			</div>		
			<?php }  ?>
        </div>
</div>
        <footer>
            <?php echo date("Y"); ?> &copy; Developed by <strong>The Website Guy.</strong>
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>
<?php $this->load->view('include/footer'); ?>