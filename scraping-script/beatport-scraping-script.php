<?php
	ini_set('max_execution_time', 3000000); //300 seconds = 5 minutes
	require 'scrape.php';
	require 'config.php';
	$obj				=	new scrape();
	$headingarray	=	array('LABEL','URL');
	$URL_LIST		=	array();
	$LABEL_LIST		=	array();
	$counter		=	0;
	$WebsiteList=$obj->selectMulti("SELECT * FROM `tbl_website_list` WHERE `website_name`='Beatport' ORDER BY row_id ASC")[0]['row_id'];
	$ListOfLabel=$obj->selectMulti("SELECT * FROM `tbl_labels_list` ORDER BY row_id ASC");
	foreach($ListOfLabel as $proURL):	
			$LABEL_LIST[]=$proURL['label_name'];
	endforeach;	
	$ListOfUrl=$obj->selectMulti("SELECT * FROM `tbl_url_list` WHERE `website_id` = $WebsiteList ORDER BY row_id ASC");
	foreach($ListOfUrl as $proURL):	
			$URL_LIST[]=$proURL['website_url'];
	endforeach;	
	for($u=0;$u<count($URL_LIST);$u++)
	{	
		$full_page		=	file_get_contents($URL_LIST[$u]);
		//$full_page		=	file_get_contents('pg.txt');
		$list			=	$obj->GetDevContent($full_page,'window.Playables =',';');
		$list			=	str_replace('window.Playables = [','',$list);
		$list			=	json_decode("[".$list."]",true);
		IF(count($list)<=0)
		{
			$list			=	$obj->GetDevContent($full_page,'window.Sliders =',';');
			$list			=	str_replace('window.Sliders = ','',$list);
			$list			=	json_decode("[".$list."]",true)[0];		
		}
		$DataArray		=	array();
		foreach($list as $row):			
			if($row['container']=='slider-latest-releases-for-genre' || $row['container']=='slider-staff-picked-releases-for-genre')
			{
				$row['url']='https://www.beatport.com'.$row['url']."?page=1&per_page=".$row['total'];
				sleep(rand(0,2));
				$output				=	$obj->pagecontentcurl($row['url']);	
				$listd				=	json_decode($obj->GetDevContent($output,'<script id="tracks">','</script>'),true);
				//$obj->pre($listd); 	exit;
				foreach($listd['tracks'] as $rows):							
								$DataArray[]=strtolower($rows['label']['name']);
				endforeach;				
			}			
		endforeach;		
		$result=array();
		for($lb=0;$lb<count($LABEL_LIST);$lb++)
		{			
			if (in_array(strtolower($LABEL_LIST[$lb]), $DataArray))
			{				
				$result[]	=	array($LABEL_LIST[$lb],$URL_LIST[$u]);
				$UPDATE="UPDATE `tbl_labels_list` SET `found_on_url` = '".$URL_LIST[$u]."' WHERE `label_name`='".$LABEL_LIST[$lb]."';";
				$obj->update($UPDATE);
				foreach($ListOfLabel as $rowed):	
					if($LABEL_LIST[$lb]==$rowed['label_name'])
					{
						$label_id=$rowed['row_id'];
					}
				endforeach;	
				$pos		=	0;
				$pos		=	array_search(strtolower($LABEL_LIST[$lb]), $DataArray)+1;
				$countquery="SELECT `row_id` FROM `tbl_labels_result` WHERE `position`=".$pos." AND `found_on_url`='".$URL_LIST[$u]."' AND `label_id`=".$label_id."";
				if($obj->countrecord($countquery)<1)
				{
					$sql="INSERT INTO `tbl_labels_result` (`label_id`,`website_id`,`position`,`found_on_url`) VALUES(".$label_id.",".$WebsiteList.",'".$pos."','".$URL_LIST[$u]."')";
					$obj->update($sql);
					$counter++;
				}
			}
		}				
	}
if($counter>0)
{
	echo 1;
}else
{
	echo 0;
}
?>