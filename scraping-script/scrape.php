<?php
class scrape 
{	
	function texttovalue($text)
	{
		return str_replace(array(" ","'"),array("-",""),strtolower($text));
	}
	function ListOfAttribute($foo,$name)
	{
		$array = array();
		preg_match( '/'.$name.'="([^"]*)"/i', $foo, $array ) ;
		return $array;
	} 
	function removeWhiteSpace($text) // Remove White Space
	{
		 $text = preg_replace('/[\t\n\r\0\x0B]/', ' ', $text);
		 $text = preg_replace('/([\s])\1+/', ' ', $text);
		 $text = trim($text);
		 return $text;
	}
	function isYTurl($aurl)
	{
		$list=array();
		//$fbUrlCheck = '/^(https?:\/\/)?(www\.)?youtube.com\/[a-zA-Z0-9(\.\?)?]/';
		$fbUrlCheck = '/(?:(?:http|https):\/\/)?(?:www.)?youtube.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/';
		$secondCheck = '/home((\/)?\.[a-zA-Z0-9])?/';	
		foreach($aurl as $url):
		$validUrl = strtolower($url);
		if(preg_match($fbUrlCheck, $validUrl) == 1 && preg_match($secondCheck, $validUrl) == 0) 
		{
			if(!in_array($validUrl,$list))
			{
				$list[]=$url;
			}
		}
		endforeach;
		$list=array_unique($list);
		if(count($list)>0)
		{
			return implode(";",$list);
		} else
		{
			return "";
		}	
	}
	function isFBurl($aurl)
	{
		$list=array();
		$fbUrlCheck = '/(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/';
		$secondCheck = '/home((\/)?\.[a-zA-Z0-9])?/';	
		foreach($aurl as $url):
		$validUrl = str_replace("http://",'https://',strtolower($url));
		if(preg_match($fbUrlCheck, $validUrl) == 1 && preg_match($secondCheck, $validUrl) == 0) 
		{
			if(!in_array($validUrl,$list))
			{
				$list[]=$validUrl;
			}
		}
		endforeach;
		$list=array_unique($list);
		if(count($list)>0)
		{
			return implode(";",$list);
		} else
		{
			return "";
		}	
	}
	/*********************************************************************************/
	function getTextBetweenTags($string, $tagname) // this will return only Tags Info like <h1></h1>
	 {
	  $pattern = "/<$tagname ?.*>(.*)<\/$tagname>/";
	  preg_match_all($pattern, $string, $matches);
	  return $matches[0];
	 }	
	function ElementValue($arr,$ele)
	{
		return $arr[$ele];
	}
	function ExplodeBy($sep,$str)
	{
		return explode($sep,$str);
	}
	function GetLastElementAarry($array)
	{
		return $array[count($array)-1];
	}
	function GetImageSrc($str)
	{
		preg_match_all('/< *img[^>]*src *= *["\']?([^"\']*)/i', $str, $images);
		return $images;
	}
	function HeadingText($str)
	{
		$headings="";
		$newstr = preg_match_all('|<h[^>]+>(.*)</h[^>]+>|iU', $str, $headings);
		return $headings[1][0];
	}
	function GetListOfEmail($text)
	{
		$arr=array();
		
		$res = preg_match_all("/[a-z0-9]+[_a-z0-9\.-]*[a-z0-9]+@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/i",$text,$matches);

		if($res) 
		{
			foreach(array_unique($matches[0]) as $email) 
			{
				$arr[]=$email;
			}
		} else
		{
			return $arr;
		}
		return $arr;	
	}	
	function FinalOptimize($ar)
	{
		$nomiarray=array();
		$isfound=0;
		$alist=array('.css','.js','.png','.jpg','.ico');
		foreach($ar as $url):
			$isfound=0;
			for($i=0;$i<count($alist);$i++)
			{			
				if(strpos($url,$alist[$i])>0) 
				{
					$isfound=1;
				}	
			}
			if($isfound==0)
			{
				$nomiarray[]=$url;
			}
		endforeach;
		return $nomiarray;
	}
	function GetSiteHost($url)
	{
		$info=parse_url($url);
		if(isset($info['host']) && $info['host']!="")
		{
			return $info['host'];
		} else
		{
			$url="http://".$url;
			$info=parse_url($url);
			return $info['host'];
		}
	}
	function MakeitfullURL($info,$site)
	{
		if(isset($info['scheme']) && $info['scheme']!="" && isset($info['path']) && $info['path']!="" && isset($info['host']) && $info['host']!="")
		{
			return true;
		}
	}
	function GetFullPageContent2($url)
	{
		return file_get_contents($url);
	}
	function getAUrls($c)
	{
		$regex = "/(?<=href=(\"|'))[^\"']+(?=(\"|'))/";
		preg_match_all($regex, $c, $matches);
		return ($matches[0]);
	}
	function getHttpUrls($c)
	{
		$regex = '/https?\:\/\/[^\" ]+/i';
		preg_match_all($regex, $c, $matches);
		return ($matches[0]);
	}
	function pre($ar)
	{
		echo "<pre>";
		print_r($ar);
		echo "</pre>";
		echo "<br/>";
	}
	function GetDevContent2($output,$dst,$dend)
	{
		$start = strpos($output, $dst);
		$end = strpos($output, $dend, $start);
		$length = $end-$start;
		$nutput = substr($output, $start, $length);
		return $nutput;
	}
	public function tdval($link)
	{
		$link = preg_replace('/<td.*?>/i', '', $link);
		$link = preg_replace('/<\/td>/i', '', $link);
		$Content = preg_replace("/&#?[a-z0-9]+;/i","",$link);
		$Content=strip_tags($Content);
		return trim(preg_replace('/\s\s+/', '',$Content));
	}
	public function linkname($link)
	{
		$link = preg_replace('/<a.*?>/i', '', $link);
		$link = preg_replace('/<\/a>/i', '', $link);
		return $link;
	}
	public function PageContentCurlProxy($url)
	{		
		$ch = curl_init();			
		if($ch === false)
		{
			die('Failed to create curl object');
		}				
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$rand=rand(0,2);
		curl_setopt($ch, CURLOPT_PROXY, $this->proxies[$rand]);
		curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
		$data = curl_exec($ch);
		$res=curl_getinfo($ch);		
		curl_close($ch);
		if($res['primary_ip']=="" || $data=="")
		{
			$this->PageContentCurlProxy($url);
		}		
		return $data; 
	}
	public function pagecontentcurl($url)
	{		
		$crl = curl_init();
		curl_setopt($crl, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
		curl_setopt($crl, CURLOPT_URL, $url);		
		curl_setopt($crl, CURLOPT_HEADER, FALSE);
		curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($crl, CURLOPT_COOKIESESSION, TRUE);		
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($crl, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($crl, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, 5);
		$ret = curl_exec($crl);		
		curl_close($crl);
		return $ret;		
	}
	function get_domain($url)
	{
	  $pieces = parse_url($url);
	  $domain = isset($pieces['host']) ? $pieces['host'] : '';
	  if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
		return "site:".$regs['domain'];
	  }
	  return false;
	}
	function GetDevContent($output,$dst,$dend)
	{
		$start = strpos($output, $dst);
		$end = strpos($output, $dend, $start);
		$length = $end-$start;
		$nutput = substr($output, $start, $length);
		return strip_tags($nutput);
	}
	function GetMetaType($des)
	{
		$listometatype=array('name="description"');
		$listometatyped=array('description');
		for($i=0;$i<count($listometatype);$i++)
		{
			$res=str_replace(strtolower($listometatype[$i]),"",strtolower($des),$countd);
			if($countd>0)
			{
				return $listometatyped[$i];
				break;
			}
		}
		return -1;
	}
	function GetSiteMeta($output)
	{
		$resultarray=array();
		$start = strpos($output, '<head>');
		$end = strpos($output, '</head>', $start);
		$length = $end-$start;
		$nutput = substr($output, $start, $length);
		$nres=explode('<meta ',$nutput);
		for($i=0;$i<count($nres);$i++)
		{			
			if(str_replace(" ","",$nres[$i])!="")
			{
				$res=$this->GetMetaType($nres[$i]);
				if($res!=-1)
				{
					$start = strpos($nres[$i], 'content=');
					$end = strpos($nres[$i], '>', $start);
					$length = $end-$start;
					$nutput = strip_tags(substr($nres[$i], $start, $length));
					$resultarray['descriptiontext']=str_replace(array($res,'/>','/','content=','"'),"",$nutput);
				}
			}			
		}		
		return $resultarray;
	}
	function update($selectquery)
		{
		global $con;
		if($result = mysqli_query($con, $selectquery) or die(mysqli_error($con, $selectquery)))
			{
				return true;
			}		
			return false;
		}
	function delete($selectquery)
		{
		global $con;
		if($result = mysqli_query($con, $selectquery) or die(mysqli_error($con, $selectquery)))
			{
				return true;
			}		
			return false;
		}
	/****************************************************DATABASE**************************************/
	public function ArrayToUpdate($tbl,$array, $where) 	
	{
        $edited = 'User Ammended';
        //$array['password'] = $this->hashPassword($array['password']);
        /*Assuming array keys are = to database fileds*/
        if (count($array) > 0) {
            foreach ($array as $key => $value) {

                $value = mysql_real_escape_string($value); // this is dedicated to @Jon
                $value = "'$value'";
                $updates[] = "$key = $value";
            }
        }
        $implodeArray = implode(', ', $updates);
        $sql = ("UPDATE $tbl SET $implodeArray WHERE $where");
        return $sql;
	}
	function ArrayToSQL($tbl_info,$insData)
	{
		global $con;
		$columns = implode("`,`",array_keys($insData));
		//$escaped_values = array_map('mysql_real_escape_string', array_values($insData));
		$escaped_values = array_map(array($con, 'real_escape_string'), array_values($insData));
		$values  = implode("','", $escaped_values);
		$sql = "INSERT INTO `$tbl_info`(`$columns`) VALUES ('$values')";
		return $sql;
	}
	
	function selectMulti($querry_sql)
	{
		global $con;
		if((@$result = mysqli_query($con, $querry_sql))==FALSE)
   		{
			$ob = new MDSchoolAdmissionCoach();
			if (DEBUG=="True")
			{
				echo $ob->mySqlMessage($querry_sql);		
			}	
		}   
		else
 		{	
	    	$count = 0;
			$data = array();
			while ( $row = mysqli_fetch_array($result)) 
			{
				$data[$count] = $row;
				$count++;
			}
			return $data;
		}
	}
	function select($selectquery)
	{
	global $con;
	/*
	$listofcar=$obj->selectMulti("SELECT * FROM ".TBL_CARS." WHERE catid='".$catdes['des']."'");
	for($i=0;$i<count($listofcar);$i++)
	{
	<?php echo $listofcar[$i]['name'];?>
	*/
	if($result = mysqli_query($con, $selectquery) or die(mysqli_error($con, $selectquery)))
		{
		return mysql_fetch_assoc($result);
		}
	else
		return false;
	}
	function countrecord($countquery)
	{
		global $con;
		if($result = mysqli_query($con, $countquery) or die(mysqli_error($con, $countquery)))
		{
			return	mysqli_affected_rows($con);
		}
		else
		{
			return false;
		}
	}
	/******************** DATABASE*******************************************************************/
}
?>