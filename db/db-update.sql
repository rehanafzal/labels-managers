ALTER TABLE `tbl_labels_result` ADD `position` INT NOT NULL AFTER `label_id`;
ALTER TABLE `tbl_labels_result` ADD `found_on_url` VARCHAR(300) NOT NULL AFTER `position`;
ALTER TABLE `tbl_website_list` ADD `run_auto` INT NOT NULL AFTER `update_date`;
ALTER TABLE `tbl_website_list` ADD `time` TIME NOT NULL AFTER `run_auto`;